package viewpoints;

import systemservices.MedicalRecordsServiceImplStub;
import customDataTypes.User;

public class MedicalRecords {
	
	public User setUser(String user, String pass) throws Exception{
		//Variable declarations
		User userInfo = new User();
		systemservices.MedicalRecordsServiceImplStub.User currentUser = new systemservices.MedicalRecordsServiceImplStub.User();
		MedicalRecordsServiceImplStub stub = new MedicalRecordsServiceImplStub();
		MedicalRecordsServiceImplStub.SetUser request = new MedicalRecordsServiceImplStub.SetUser();
		//Set method stub parameters
		request.setUser(user);
		request.setPass(pass);
		MedicalRecordsServiceImplStub.SetUserResponse responce = stub.setUser(request);
		//Get method stub return
		currentUser = responce.get_return();
		//Convert variable
		userInfo.setUserClinic(currentUser.getUserClinic());
		userInfo.setUserId(currentUser.getUserId());
		userInfo.setUserName(currentUser.getUserName());
		userInfo.setUserSurname(currentUser.getUserSurname());
		userInfo.setUserType(currentUser.getUserType());
		return userInfo;
	}
	
	public User getUser(String userId) throws Exception{
		//Variable declarations
		User userInfo = new User();
		systemservices.MedicalRecordsServiceImplStub.User currentUser = new systemservices.MedicalRecordsServiceImplStub.User();
		MedicalRecordsServiceImplStub stub = new MedicalRecordsServiceImplStub();
		MedicalRecordsServiceImplStub.GetUser request = new MedicalRecordsServiceImplStub.GetUser();
		//Set method stub parameters
		request.setUserId(userId);
		MedicalRecordsServiceImplStub.GetUserResponse responce = stub.getUser(request);
		//Get method stub return
		currentUser = responce.get_return();
		//Convert variable
		userInfo.setUserClinic(currentUser.getUserClinic());
		userInfo.setUserId(currentUser.getUserId());
		userInfo.setUserName(currentUser.getUserName());
		userInfo.setUserSurname(currentUser.getUserSurname());
		userInfo.setUserType(currentUser.getUserType());
		return userInfo;
	}
	
	public String setRequest(String request){
		String responce = null;
		//TODO Web Service method
		return responce;
	}
	
	public void addTransaction(String transaction){
		//TODO Web Service method
	}
}
