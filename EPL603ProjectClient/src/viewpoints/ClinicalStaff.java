package viewpoints;

import systemservices.ClinicalStaffServiceImplStub;
import systemservices.ClinicalStaffServiceImplStub.SetDBSessionInfoResponse;
import customDataTypes.Patient;
import customDataTypes.Session;
import customDataTypes.Treatments;


public class ClinicalStaff {
	
		
	public Patient getPatientRecord(int patientId) throws Exception{
		//Variable declarations
		Patient patient = new Patient();
		systemservices.ClinicalStaffServiceImplStub.Asthenis currentPatient = new systemservices.ClinicalStaffServiceImplStub.Asthenis();
		ClinicalStaffServiceImplStub stub = new ClinicalStaffServiceImplStub();
		ClinicalStaffServiceImplStub.GetDBPatientRecord request = new ClinicalStaffServiceImplStub.GetDBPatientRecord();
		//Set method stub parameters
		request.setPatientId(patientId);
		ClinicalStaffServiceImplStub.GetDBPatientRecordResponse responce = stub.getDBPatientRecord(request);
		//Get method stub return
		currentPatient = responce.get_return();
		//Convert variable
		patient.setPatientId(currentPatient.getAsthenisId());
		patient.setPatientName(currentPatient.getAsthenisName());
		patient.setPatientSurname(currentPatient.getAsthenisSurname());
		patient.setPatientAddress(currentPatient.getAsthenisAddress());
		patient.setPatientAllergies(currentPatient.getAsthenisAllergies());
		patient.setPatientPhone(currentPatient.getAsthenisPhone());
		patient.setPatientRiskStatus(currentPatient.getAsthenisRiskStatus());
		patient.setPatientStatus(currentPatient.getAsthenisStatus());
		return patient;
	}
	
	public Treatments getTreatmentsInfo() throws Exception{ 
		//Variable declarations
		String requestString = "request";
		Treatments treatments = new Treatments();
		systemservices.ClinicalStaffServiceImplStub.Treatments currentTreatments = new systemservices.ClinicalStaffServiceImplStub.Treatments();
		ClinicalStaffServiceImplStub stub = new ClinicalStaffServiceImplStub();
		ClinicalStaffServiceImplStub.GetDBTreatmentsInfo request = new ClinicalStaffServiceImplStub.GetDBTreatmentsInfo();
		//Set method stub parameters
		request.setRequest(requestString);
		ClinicalStaffServiceImplStub.GetDBTreatmentsInfoResponse responce = stub.getDBTreatmentsInfo(request);
		//Get method stub return
		currentTreatments = responce.get_return();
		//Convert variable
		treatments.setDrug(currentTreatments.getDrug());
		treatments.setSideEffects(currentTreatments.getSideEffects());
		return treatments;
	}
	
	public Session getSessionInfo(int patientId, String sessionDate) throws Exception{
		//Variable declarations
		Session session = new Session();
		ClinicalStaffServiceImplStub.Randevu currentSession = new ClinicalStaffServiceImplStub.Randevu();
		ClinicalStaffServiceImplStub stub = new ClinicalStaffServiceImplStub();
		ClinicalStaffServiceImplStub.GetDBSessionInfo request = new ClinicalStaffServiceImplStub.GetDBSessionInfo();
		//Set method stub parameters
		request.setPatientId(patientId);
		request.setSessionDate(sessionDate);
		ClinicalStaffServiceImplStub.GetDBSessionInfoResponse responce = stub.getDBSessionInfo(request);
		//Get method stub return
		currentSession = responce.get_return();
		//Convert variable
		session.setSessionCondition(currentSession.getRandevuCondition());
		session.setSessionDate(currentSession.getRandevuDate());
		session.setSessionId(currentSession.getRandevuId());
		session.setSessionNotes(currentSession.getRandevuNotes());
		ClinicalStaffServiceImplStub.Treatments treat = currentSession.getRandevuTreatment();
		Treatments treatm = new Treatments();
		treatm.setDrug(treat.getDrug());
		treatm.setSideEffects(treat.getSideEffects());
		session.setSessionTreatment(treatm);
		session.setSessionUptodate(currentSession.getRandevuUptodate());
		session.setUserId(currentSession.getUserId());
		session.setUserName(currentSession.getUserNames());
		return session;
	}
	
	public String setSessionInfo(Session session, int patientId, String userId) throws Exception{
		//Variable declarations
		ClinicalStaffServiceImplStub.Randevu currentSession = new ClinicalStaffServiceImplStub.Randevu();
		ClinicalStaffServiceImplStub stub = new ClinicalStaffServiceImplStub();
		ClinicalStaffServiceImplStub.SetDBSessionInfo request = new ClinicalStaffServiceImplStub.SetDBSessionInfo();
		//Convert variable
		currentSession.setRandevuCondition(session.getSessionCondition());
		currentSession.setRandevuDate(session.getSessionDate());
		currentSession.setRandevuId(session.getSessionId());
		currentSession.setRandevuNotes(session.getSessionNotes());
		Treatments treat = session.getSessionTreatment();
		ClinicalStaffServiceImplStub.Treatments treatm = new ClinicalStaffServiceImplStub.Treatments();
		treatm.setDrug(treat.getDrug());
		treatm.setSideEffects(treat.getSideEffects());
		currentSession.setRandevuTreatment(treatm);
		currentSession.setRandevuUptodate(session.isSessionUptodate());
		currentSession.setUserId(session.getUserId());
		currentSession.setUserNames(session.getUserNames());
		//Set method stub parameters
		request.setPatientId(patientId);
		request.setSession(currentSession);
		request.setUserId(userId);
		SetDBSessionInfoResponse responce = stub.setDBSessionInfo(request);
		//Get method stub return
		String sResponce = responce.get_return();
		return sResponce;
	}

	
}
