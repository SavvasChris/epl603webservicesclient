package viewpoints;

import systemservices.ReceptionistsServiceImplStub;
import systemservices.ReceptionistsServiceImplStub.SetDBPatientAppointmentResponse;
import systemservices.ReceptionistsServiceImplStub.SetDBPatientResponse;
import customDataTypes.Appointment;
import customDataTypes.Patient;
import customDataTypes.User;

public class Receptionists {
	
	public Appointment[] getAppointments(String date) throws Exception{
		//Variable declarations
		Appointment[] appointment;
		systemservices.ReceptionistsServiceImplStub.Kratisi[] currentAppointment; 
		ReceptionistsServiceImplStub stub = new ReceptionistsServiceImplStub();
		ReceptionistsServiceImplStub.GetDBAppointments request = new ReceptionistsServiceImplStub.GetDBAppointments();
		//Set method stub parameters
		request.setDate(date);
		ReceptionistsServiceImplStub.GetDBAppointmentsResponse responce = stub.getDBAppointments(request);
		//Get method stub return
		currentAppointment = responce.get_return();
		appointment = new Appointment[currentAppointment.length];
		//Convert variable
		for(int i=0; i<currentAppointment.length; i++){
			appointment[i] = new Appointment();
			appointment[i].setAppointmentDate(currentAppointment[i].getKratisiDate());
			Patient patient = new Patient();
			ReceptionistsServiceImplStub.Asthenis currentPatient = currentAppointment[i].getKratisiPatient();
			patient.setPatientId(currentPatient.getAsthenisId());
			patient.setPatientName(currentPatient.getAsthenisName());
			patient.setPatientSurname(currentPatient.getAsthenisSurname());
			patient.setPatientAddress(currentPatient.getAsthenisAddress());
			patient.setPatientAllergies(currentPatient.getAsthenisAllergies());
			patient.setPatientPhone(currentPatient.getAsthenisPhone());
			patient.setPatientRiskStatus(currentPatient.getAsthenisRiskStatus());
			patient.setPatientStatus(currentPatient.getAsthenisStatus());
			appointment[i].setAppointmentPatient(patient);
			appointment[i].setAppointmentStatus(currentAppointment[i].getKratisiStatus());
			User userInfo = new User();
			ReceptionistsServiceImplStub.User currentUser = currentAppointment[i].getKratisiUser();
			userInfo.setUserClinic(currentUser.getUserClinic());
			userInfo.setUserId(currentUser.getUserId());
			userInfo.setUserName(currentUser.getUserName());
			userInfo.setUserSurname(currentUser.getUserSurname());
			userInfo.setUserType(currentUser.getUserType());
			appointment[i].setAppointmentUser(userInfo);
		}
		return appointment;
	}
	
	public Patient getPatientRecord(int patientId) throws Exception{
		//Variable declarations
		Patient patient = new Patient();
		systemservices.ReceptionistsServiceImplStub.Asthenis currentPatient = new systemservices.ReceptionistsServiceImplStub.Asthenis();
		ReceptionistsServiceImplStub stub = new ReceptionistsServiceImplStub();
		ReceptionistsServiceImplStub.GetDBPatientRecord request = new ReceptionistsServiceImplStub.GetDBPatientRecord();
		//Set method stub parameters
		request.setPatientId(patientId);
		ReceptionistsServiceImplStub.GetDBPatientRecordResponse responce = stub.getDBPatientRecord(request);
		//Get method stub return
		currentPatient = responce.get_return();
		//Convert variable
		patient.setPatientId(currentPatient.getAsthenisId());
		patient.setPatientName(currentPatient.getAsthenisName());
		patient.setPatientSurname(currentPatient.getAsthenisSurname());
		patient.setPatientAddress(currentPatient.getAsthenisAddress());
		patient.setPatientAllergies(currentPatient.getAsthenisAllergies());
		patient.setPatientPhone(currentPatient.getAsthenisPhone());
		patient.setPatientRiskStatus(currentPatient.getAsthenisRiskStatus());
		patient.setPatientStatus(currentPatient.getAsthenisStatus());
		return patient;
	}
	
	public String setPatientAppointment(Appointment appointment) throws Exception{
		//Variable declarations
		ReceptionistsServiceImplStub.Kratisi currentAppointment = new ReceptionistsServiceImplStub.Kratisi();
		ReceptionistsServiceImplStub stub = new ReceptionistsServiceImplStub();
		ReceptionistsServiceImplStub.SetDBPatientAppointment request = new ReceptionistsServiceImplStub.SetDBPatientAppointment();
		//Convert variable
		currentAppointment.setKratisiDate(appointment.getAppointmentDate());
		Patient currentPatient = appointment.getAppointmentPatient();
		ReceptionistsServiceImplStub.Asthenis patient = new ReceptionistsServiceImplStub.Asthenis();
		patient.setAsthenisId(currentPatient.getPatientId());
		patient.setAsthenisName(currentPatient.getPatientName());
		patient.setAsthenisSurname(currentPatient.getPatientSurname());
		patient.setAsthenisAddress(currentPatient.getPatientAddress());
		patient.setAsthenisAllergies(currentPatient.getPatientAllergies());
		patient.setAsthenisPhone(currentPatient.getPatientPhone());
		patient.setAsthenisRiskStatus(currentPatient.getPatientRiskStatus());
		patient.setAsthenisStatus(currentPatient.isPatientStatus());
		currentAppointment.setKratisiPatient(patient);
		currentAppointment.setKratisiStatus(appointment.getAppointmentStatus());
		User currentUser = appointment.getAppointmentUser();
		ReceptionistsServiceImplStub.User userInfo = new ReceptionistsServiceImplStub.User();
		userInfo.setUserClinic(currentUser.getUserClinic());
		userInfo.setUserId(currentUser.getUserId());
		userInfo.setUserName(currentUser.getUserName());
		userInfo.setUserSurname(currentUser.getUserSurname());
		userInfo.setUserType(currentUser.getUserType());
		currentAppointment.setKratisiUser(userInfo);
		//Set method stub parameters
		request.setAppointment(currentAppointment);
		SetDBPatientAppointmentResponse responce = stub.setDBPatientAppointment(request);
		//Get method stub return
		String sResponce = responce.get_return();
		return sResponce;
	}
	
	public String setPatient(Patient patient) throws Exception{
		//Variable declarations
		ReceptionistsServiceImplStub.Asthenis currentPatient = new ReceptionistsServiceImplStub.Asthenis();
		ReceptionistsServiceImplStub stub = new ReceptionistsServiceImplStub();
		ReceptionistsServiceImplStub.SetDBPatient request = new ReceptionistsServiceImplStub.SetDBPatient();
		//Convert variable
		currentPatient.setAsthenisAddress(patient.getPatientAddress());
		currentPatient.setAsthenisAllergies(patient.getPatientAllergies());
		currentPatient.setAsthenisId(patient.getPatientId());
		currentPatient.setAsthenisName(patient.getPatientName());
		currentPatient.setAsthenisSurname(patient.getPatientSurname());
		currentPatient.setAsthenisPhone(patient.getPatientPhone());
		currentPatient.setAsthenisRiskStatus(patient.getPatientRiskStatus());
		currentPatient.setAsthenisStatus(patient.isPatientStatus());
		//Set method stub parameters
		request.setPatient(currentPatient);
		SetDBPatientResponse responce = stub.setDBPatient(request);
		//Get method stub return
		String sResponce = responce.get_return();
		return sResponce;
	}
	
}
