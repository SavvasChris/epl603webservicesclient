package viewpoints;

import systemservices.ManagementServiceImplStub;
import customDataTypes.ConditionsAndTreatments;
import customDataTypes.Report;
import customDataTypes.ReportData;

public class Management {
	
	public ConditionsAndTreatments getPatientsConditionsAndTreatments() throws Exception{
		//Variable declarations
		String requestString = "request";
		ConditionsAndTreatments conditionsAndTreatments = new ConditionsAndTreatments();
		ManagementServiceImplStub.ConditionsAndTreatments currentConditionsAndTreatments = new ManagementServiceImplStub.ConditionsAndTreatments();
		ManagementServiceImplStub stub = new ManagementServiceImplStub();
		ManagementServiceImplStub.GetDBPatientsConditionsAndTreatments request = new ManagementServiceImplStub.GetDBPatientsConditionsAndTreatments();
		//Set method stub parameters
		request.setRequest(requestString);
		ManagementServiceImplStub.GetDBPatientsConditionsAndTreatmentsResponse responce = stub.getDBPatientsConditionsAndTreatments(request);
		//Get method stub return
		currentConditionsAndTreatments = responce.get_return();
		//Convert variable
		conditionsAndTreatments.setConditions(currentConditionsAndTreatments.getConditions());
		ManagementServiceImplStub.Treatments treat =  currentConditionsAndTreatments.getTreatments();
		customDataTypes.Treatments treatm = new customDataTypes.Treatments();
		treatm.setDrug(treat.getDrug());
		treatm.setSideEffects(treat.getSideEffects());
		conditionsAndTreatments.setTreatments(treatm);
		return conditionsAndTreatments;
	}
	
	private ReportData getNumberOfPatientsEachDay() throws Exception{
		//Variable declarations
		String requestString = "request";
		ReportData reportData = new ReportData();
		ManagementServiceImplStub.ReportData currentReportData = new ManagementServiceImplStub.ReportData();
		ManagementServiceImplStub stub = new ManagementServiceImplStub();
		ManagementServiceImplStub.GetDBNumberOfPatientsEachDay request = new ManagementServiceImplStub.GetDBNumberOfPatientsEachDay();
		//Set method stub parameters
		request.setRequest(requestString);
		ManagementServiceImplStub.GetDBNumberOfPatientsEachDayResponse responce = stub.getDBNumberOfPatientsEachDay(request);
		//Get method stub return
		currentReportData = responce.get_return();
		//Convert variable
		reportData.setName(currentReportData.getName());
		reportData.setNumber(currentReportData.getNumber());
		reportData.setDate(currentReportData.getDate());
		return reportData;
	}
	
	private ReportData getNumberOfPatientsEachMonth() throws Exception{
		//Variable declarations
		String requestString = "request";
		ReportData reportData = new ReportData();
		ManagementServiceImplStub.ReportData currentReportData = new ManagementServiceImplStub.ReportData();
		ManagementServiceImplStub stub = new ManagementServiceImplStub();
		ManagementServiceImplStub.GetDBNumberOfPatientsEachMonth request = new ManagementServiceImplStub.GetDBNumberOfPatientsEachMonth();
		//Set method stub parameters
		request.setRequest(requestString);
		ManagementServiceImplStub.GetDBNumberOfPatientsEachMonthResponse responce = stub.getDBNumberOfPatientsEachMonth(request);
		//Get method stub return
		currentReportData = responce.get_return();
		//Convert variable
		reportData.setName(currentReportData.getName());
		reportData.setNumber(currentReportData.getNumber());
		reportData.setDate(currentReportData.getDate());
		return reportData;
	}
	
	private ReportData getNumberOfPatientsEachCondition() throws Exception{
		//Variable declarations
		String requestString = "request";
		ReportData reportData = new ReportData();
		ManagementServiceImplStub.ReportData currentReportData = new ManagementServiceImplStub.ReportData();
		ManagementServiceImplStub stub = new ManagementServiceImplStub();
		ManagementServiceImplStub.GetDBNumberOfPatientsEachCondition request = new ManagementServiceImplStub.GetDBNumberOfPatientsEachCondition();
		//Set method stub parameters
		request.setRequest(requestString);
		ManagementServiceImplStub.GetDBNumberOfPatientsEachConditionResponse responce = stub.getDBNumberOfPatientsEachCondition(request);
		//Get method stub return
		currentReportData = responce.get_return();
		//Convert variable
		reportData.setName(currentReportData.getName());
		reportData.setNumber(currentReportData.getNumber());
		reportData.setDate(currentReportData.getDate());
		return reportData;
	}
	
	private ReportData getNumberOfEachDrugPrescribed() throws Exception{
		//Variable declarations
		String requestString = "request";
		ReportData reportData = new ReportData();
		ManagementServiceImplStub.ReportData currentReportData = new ManagementServiceImplStub.ReportData();
		ManagementServiceImplStub stub = new ManagementServiceImplStub();
		ManagementServiceImplStub.GetDBNumberOfEachDrugPrescribed request = new ManagementServiceImplStub.GetDBNumberOfEachDrugPrescribed();
		//Set method stub parameters
		request.setRequest(requestString);
		ManagementServiceImplStub.GetDBNumberOfEachDrugPrescribedResponse responce = stub.getDBNumberOfEachDrugPrescribed(request);
		//Get method stub return
		currentReportData = responce.get_return();
		//Convert variable
		reportData.setName(currentReportData.getName());
		reportData.setNumber(currentReportData.getNumber());
		reportData.setDate(currentReportData.getDate());
		return reportData;
	}
	
	public Report createReport() throws Exception{
		Report report = new Report();
		report.numberOfEachDrugPrescribed = getNumberOfEachDrugPrescribed();
		report.numberOfPatientsEachCondition = getNumberOfPatientsEachCondition();
		report.numberOfPatientsEachDay = getNumberOfPatientsEachDay();
		report.numberOfPatientsEachMonth = getNumberOfPatientsEachMonth();
		return report;
		
	}
	
}
