
/**
 * ReceptionistsServiceImplCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package systemservices;

    /**
     *  ReceptionistsServiceImplCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ReceptionistsServiceImplCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ReceptionistsServiceImplCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ReceptionistsServiceImplCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
               // No methods generated for meps other than in-out
                
           /**
            * auto generated Axis2 call back method for setDBPatientAppointment method
            * override this method for handling normal response from setDBPatientAppointment operation
            */
           public void receiveResultsetDBPatientAppointment(
                    systemservices.ReceptionistsServiceImplStub.SetDBPatientAppointmentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setDBPatientAppointment operation
           */
            public void receiveErrorsetDBPatientAppointment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBPatientRecord method
            * override this method for handling normal response from getDBPatientRecord operation
            */
           public void receiveResultgetDBPatientRecord(
                    systemservices.ReceptionistsServiceImplStub.GetDBPatientRecordResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBPatientRecord operation
           */
            public void receiveErrorgetDBPatientRecord(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBAppointments method
            * override this method for handling normal response from getDBAppointments operation
            */
           public void receiveResultgetDBAppointments(
                    systemservices.ReceptionistsServiceImplStub.GetDBAppointmentsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBAppointments operation
           */
            public void receiveErrorgetDBAppointments(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for setDBPatient method
            * override this method for handling normal response from setDBPatient operation
            */
           public void receiveResultsetDBPatient(
                    systemservices.ReceptionistsServiceImplStub.SetDBPatientResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setDBPatient operation
           */
            public void receiveErrorsetDBPatient(java.lang.Exception e) {
            }
                


    }
    