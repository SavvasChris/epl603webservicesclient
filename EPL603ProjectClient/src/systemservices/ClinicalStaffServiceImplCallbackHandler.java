
/**
 * ClinicalStaffServiceImplCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package systemservices;

    /**
     *  ClinicalStaffServiceImplCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ClinicalStaffServiceImplCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ClinicalStaffServiceImplCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ClinicalStaffServiceImplCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getDBSessionInfo method
            * override this method for handling normal response from getDBSessionInfo operation
            */
           public void receiveResultgetDBSessionInfo(
                    systemservices.ClinicalStaffServiceImplStub.GetDBSessionInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBSessionInfo operation
           */
            public void receiveErrorgetDBSessionInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBPatientRecord method
            * override this method for handling normal response from getDBPatientRecord operation
            */
           public void receiveResultgetDBPatientRecord(
                    systemservices.ClinicalStaffServiceImplStub.GetDBPatientRecordResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBPatientRecord operation
           */
            public void receiveErrorgetDBPatientRecord(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBTreatmentsInfo method
            * override this method for handling normal response from getDBTreatmentsInfo operation
            */
           public void receiveResultgetDBTreatmentsInfo(
                    systemservices.ClinicalStaffServiceImplStub.GetDBTreatmentsInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBTreatmentsInfo operation
           */
            public void receiveErrorgetDBTreatmentsInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for setDBSessionInfo method
            * override this method for handling normal response from setDBSessionInfo operation
            */
           public void receiveResultsetDBSessionInfo(
                    systemservices.ClinicalStaffServiceImplStub.SetDBSessionInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setDBSessionInfo operation
           */
            public void receiveErrorsetDBSessionInfo(java.lang.Exception e) {
            }
                


    }
    