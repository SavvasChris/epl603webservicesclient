
/**
 * ManagementServiceImplCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package systemservices;

    /**
     *  ManagementServiceImplCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ManagementServiceImplCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ManagementServiceImplCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ManagementServiceImplCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getDBNumberOfPatientsEachCondition method
            * override this method for handling normal response from getDBNumberOfPatientsEachCondition operation
            */
           public void receiveResultgetDBNumberOfPatientsEachCondition(
                    systemservices.ManagementServiceImplStub.GetDBNumberOfPatientsEachConditionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBNumberOfPatientsEachCondition operation
           */
            public void receiveErrorgetDBNumberOfPatientsEachCondition(java.lang.Exception e) {
            }
                
               // No methods generated for meps other than in-out
                
           /**
            * auto generated Axis2 call back method for getDBPatientsConditionsAndTreatments method
            * override this method for handling normal response from getDBPatientsConditionsAndTreatments operation
            */
           public void receiveResultgetDBPatientsConditionsAndTreatments(
                    systemservices.ManagementServiceImplStub.GetDBPatientsConditionsAndTreatmentsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBPatientsConditionsAndTreatments operation
           */
            public void receiveErrorgetDBPatientsConditionsAndTreatments(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBNumberOfEachDrugPrescribed method
            * override this method for handling normal response from getDBNumberOfEachDrugPrescribed operation
            */
           public void receiveResultgetDBNumberOfEachDrugPrescribed(
                    systemservices.ManagementServiceImplStub.GetDBNumberOfEachDrugPrescribedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBNumberOfEachDrugPrescribed operation
           */
            public void receiveErrorgetDBNumberOfEachDrugPrescribed(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBNumberOfPatientsEachDay method
            * override this method for handling normal response from getDBNumberOfPatientsEachDay operation
            */
           public void receiveResultgetDBNumberOfPatientsEachDay(
                    systemservices.ManagementServiceImplStub.GetDBNumberOfPatientsEachDayResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBNumberOfPatientsEachDay operation
           */
            public void receiveErrorgetDBNumberOfPatientsEachDay(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDBNumberOfPatientsEachMonth method
            * override this method for handling normal response from getDBNumberOfPatientsEachMonth operation
            */
           public void receiveResultgetDBNumberOfPatientsEachMonth(
                    systemservices.ManagementServiceImplStub.GetDBNumberOfPatientsEachMonthResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDBNumberOfPatientsEachMonth operation
           */
            public void receiveErrorgetDBNumberOfPatientsEachMonth(java.lang.Exception e) {
            }
                


    }
    