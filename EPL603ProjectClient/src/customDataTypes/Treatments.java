package customDataTypes;

public class Treatments {
	private String[] drug;
	private String[] sideEffects;
	
	public String[] getDrug() {
		return drug;
	}
	public void setDrug(String[] drug) {
		this.drug = drug;
	}
	public String[] getSideEffects() {
		return sideEffects;
	}
	public void setSideEffects(String[] sideEffects) {
		this.sideEffects = sideEffects;
	}
	
}
