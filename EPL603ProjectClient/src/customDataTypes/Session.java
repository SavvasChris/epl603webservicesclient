package customDataTypes;

public class Session {
	private int sessionId;
	private String sessionCondition;
	private Treatments sessionTreatment;
	private String sessionDate;
	private int sessionUptodate;
	private String sessionNotes;
	private String userNames;
	private String userId;
	
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public String getSessionDate() {
		return sessionDate;
	}
	public void setSessionDate(String sessionDate) {
		this.sessionDate = sessionDate;
	}
	public int isSessionUptodate() {
		return sessionUptodate;
	}
	public void setSessionUptodate(int sessionUptodate) {
		this.sessionUptodate = sessionUptodate;
	}
	public String getSessionNotes() {
		return sessionNotes;
	}
	public void setSessionNotes(String sessionNotes) {
		this.sessionNotes = sessionNotes;
	}
	public String getUserNames() {
		return userNames;
	}
	public void setUserName(String userNames) {
		this.userNames = userNames;
	}
	
	public String getSessionCondition() {
		return sessionCondition;
	}
	public void setSessionCondition(String sessionCondition) {
		this.sessionCondition = sessionCondition;
	}
	public Treatments getSessionTreatment() {
		return this.sessionTreatment;
	}
	public void setSessionTreatment(Treatments sessionTreatment) {
		this.sessionTreatment = sessionTreatment;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
}
