package mainentry;

import viewpoints.MedicalRecords;
import Controllers.LoginUIController;
import Views.LoginUIView;

public class Main {

	public static void main(String[] args) {
		
		MedicalRecords model = new MedicalRecords();
		LoginUIView view = new LoginUIView();
		LoginUIController controller = new LoginUIController(model, view);
		
	}

}
