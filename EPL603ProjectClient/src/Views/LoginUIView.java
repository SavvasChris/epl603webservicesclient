package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;


import customDataTypes.LoginInfo;
import viewpoints.MedicalRecords;

public class LoginUIView {

	private JFrame frame;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JButton btnLogin = new JButton("Login");

	/**
	 * Create the application.
	 */
	public LoginUIView() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Login to the System!");
		lblNewLabel.setBounds(68, 11, 249, 25);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		txtUsername = new JTextField();
		txtUsername.setBounds(176, 92, 141, 20);
		txtUsername.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setBounds(68, 133, 77, 14);
		
		JLabel lblNewLabel_1 = new JLabel("Username");
		lblNewLabel_1.setBounds(66, 95, 79, 14);
		
		txtPassword = new JTextField();
		txtPassword.setBounds(176, 130, 141, 20);
		txtPassword.setColumns(10);
		
		
		btnLogin.setBounds(121, 173, 141, 23);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(lblNewLabel);
		frame.getContentPane().add(txtUsername);
		frame.getContentPane().add(lblNewLabel_2);
		frame.getContentPane().add(lblNewLabel_1);
		frame.getContentPane().add(txtPassword);
		frame.getContentPane().add(btnLogin);
		
	}
	public void addActionListenerbtnLogin(ActionListener e){
		btnLogin.setActionCommand("Login");
		btnLogin.addActionListener(e);
	}

	public LoginInfo getLoginFormData(){
		LoginInfo requestedLogin = new LoginInfo();
		
		requestedLogin.setUsername(txtUsername.getText());
		requestedLogin.setPassword(txtPassword.getText());
		
		return requestedLogin;
	}
	
	public void setVisible(boolean visible){
		frame.setVisible(visible);
	}
	public boolean Validate(){
		try{
			if (txtUsername.getText().isEmpty() || txtPassword.getText().isEmpty()){
				this.showError("You have to complete all fields to login", "Error");
				return false;
			}
			
		}catch (Exception e){
			this.showError("There seems to be an unexpected error here, did you try something sneaky?", "Fatal Error");
		}
		return true;
	}
	
	public void showError(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.ERROR_MESSAGE);
	}
}
