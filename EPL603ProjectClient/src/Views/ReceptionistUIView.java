package Views;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;

import customDataTypes.Appointment;
import customDataTypes.Patient;
import customDataTypes.User;

import javax.swing.JPanel;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.List;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.plaf.SliderUI;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class ReceptionistUIView {

	User user = new User();
	Patient patient = new Patient();
	ArrayList<customDataTypes.Appointment> appointment = new ArrayList<Appointment>(); //keep all appointemnts
	
	private static final boolean Appointment = false;
	private JFrame frame;
	private JList<String> lsAppointments = new JList();
	private JButton btnAddNewAppointment = new JButton("Add New Appointment");
	private JButton btnPrintPrescription = new JButton("Print Prescription");
	private JButton btnCheckDate = new JButton("Check Date");
	private final JPanel appointmentsCental = new JPanel();
	private JButton btnSearchStaff = new JButton("Search Staff");
	private JButton btnSaveAppointment = new JButton("Save Appointment");
	private JButton btnClose = new JButton("Close");
	private JTextField txtSearchStaff = new JTextField();
	private JLabel lblPatientName = new JLabel("New label");
	private JLabel lblStaffName = new JLabel("New label");
	private JButton btnAddPatient = new JButton("Add Patient");
	private JButton btnSearchPatient = new JButton("Search Patient");
	private final JTextField txtSearchPatient = new JTextField();
	private final JPanel AddPatient = new JPanel();
	private final JLabel label_3 = new JLabel("Add New Patient");
	private final JLabel label_4 = new JLabel("Name");
	private final JLabel label_5 = new JLabel("Surname");
	private final JLabel label_6 = new JLabel("Address");
	private final JLabel label_7 = new JLabel("Phone");
	private final JLabel label_8 = new JLabel("Risk Status");
	private final JLabel label_9 = new JLabel("Patient Knowed Alergies");
	private final JLabel label_10 = new JLabel("Patient Status");
	private final JSlider sdPatientRisk = new JSlider();
	private final JTextField txtPatientName = new JTextField();
	private final JTextField txtPatientSurname = new JTextField();
	private final JTextField txtPatientAddress = new JTextField();
	private final JTextField txtPatientPhone = new JTextField();
	private final JTextField txtPatientAlergies = new JTextField();
	private final JButton btnCreatePatient = new JButton("Create Patient");
	private final JButton btnCloseAddPatient = new JButton("Close");
	private final JCheckBox cbPatientStatus = new JCheckBox("Alive");
	private final JLabel lblNewLabel_1 = new JLabel("Patient Civic ID");
	private final JTextField txtPatientId = new JTextField();
	private final JButton btnEditAppointment = new JButton("Edit Appointment");
	private final JPanel EditAppointment = new JPanel();
	private final JLabel lblNewLabel_2 = new JLabel("Date");
	private final JLabel lblNewLabel_3 = new JLabel("Patient Name");
	private final JLabel lblNewLabel_4 = new JLabel("Staff Name");
	private final JLabel lblNewLabel_5 = new JLabel("Status");
	private final JButton btnUpdate = new JButton("Update");
	private final JButton btnCancel = new JButton("Cancel");
	private final JLabel lblUpdateDate = new JLabel("New label");
	private final JLabel lblUpdatePatient = new JLabel("New label");
	private final JLabel lblUpdateStaff = new JLabel("New label");
	private final JTextField txtUpdateStatus = new JTextField();
	private final JComboBox cbYear = new JComboBox();
	private final JComboBox cbMonth = new JComboBox();
	private final JComboBox cbDays = new JComboBox();
	private final JComboBox cbYearAddAppointment = new JComboBox();
	private final JComboBox cbMonthAddAppointment = new JComboBox();
	private final JComboBox cbDayAddAppointment = new JComboBox();
	public ReceptionistUIView() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		txtUpdateStatus.setText("");
		txtUpdateStatus.setBounds(183, 182, 197, 20);
		txtUpdateStatus.setColumns(10);
		txtPatientId.setBounds(246, 47, 211, 20);
		txtPatientId.setColumns(10);
		txtSearchPatient.setBounds(54, 34, 153, 20);
		txtSearchPatient.setColumns(10);
		frame = new JFrame();
		frame.setBounds(100, 100, 668, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		frame.getContentPane().add(appointmentsCental, "Appointment");
		appointmentsCental.setLayout(null);
		lsAppointments.setForeground(Color.WHITE);
		lsAppointments.setBounds(35, 112, 575, 194);
		appointmentsCental.add(lsAppointments);
		
		
		lsAppointments.setBackground(Color.DARK_GRAY);
		
		JLabel lblNewLabel = new JLabel("Appointments");
		lblNewLabel.setBounds(35, 87, 170, 14);
		appointmentsCental.add(lblNewLabel);
		btnAddNewAppointment.setBounds(35, 385, 147, 23);
		appointmentsCental.add(btnAddNewAppointment);
		
		btnPrintPrescription.setBounds(351, 385, 170, 23);
		btnPrintPrescription.setVisible(false);
		
		appointmentsCental.add(btnPrintPrescription);
		btnCheckDate.setBounds(336, 27, 89, 23);
		appointmentsCental.add(btnCheckDate);
		
		JLabel lblInsertDate = new JLabel("Insert Date");
		lblInsertDate.setBounds(23, 31, 129, 14);
		appointmentsCental.add(lblInsertDate);
		
		
		btnEditAppointment.setBounds(192, 385, 149, 23);
		appointmentsCental.add(btnEditAppointment);
		
		cbYear.setModel(new DefaultComboBoxModel(new String[] {"1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"}));
		cbYear.setBounds(124, 28, 58, 20);
		
		appointmentsCental.add(cbYear);
		cbMonth.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		cbMonth.setBounds(192, 28, 50, 20);
		
		appointmentsCental.add(cbMonth);
		cbDays.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		cbDays.setBounds(252, 28, 50, 20);
		
		appointmentsCental.add(cbDays);
		
		JPanel AddAppointemnt = new JPanel();
		frame.getContentPane().add(AddAppointemnt, "AddAppointment");
		AddAppointemnt.setLayout(null);
		
		
		btnSearchPatient.setBounds(217, 33, 110, 23);
		AddAppointemnt.add(btnSearchPatient);
		
		JLabel label = new JLabel("Date");
		label.setBounds(51, 127, 46, 14);
		AddAppointemnt.add(label);
		
		
		btnAddPatient.setBounds(337, 33, 173, 23);
		AddAppointemnt.add(btnAddPatient);
		
		JLabel label_1 = new JLabel("Patient Name");
		label_1.setBounds(51, 166, 89, 14);
		AddAppointemnt.add(label_1);
		
		
		btnSearchStaff.setBounds(217, 67, 110, 23);
		AddAppointemnt.add(btnSearchStaff);
		
		JLabel label_2 = new JLabel("Staff Name");
		label_2.setBounds(51, 202, 73, 14);
		AddAppointemnt.add(label_2);
		
		
		btnSaveAppointment.setBounds(51, 357, 135, 23);
		AddAppointemnt.add(btnSaveAppointment);
		
		
		btnClose.setBounds(196, 357, 89, 23);
		AddAppointemnt.add(btnClose);
		
		
		txtSearchStaff.setBounds(52, 65, 155, 20);
		AddAppointemnt.add(txtSearchStaff);
		txtSearchStaff.setColumns(10);
		
		
		lblPatientName.setBounds(136, 166, 149, 14);
		AddAppointemnt.add(lblPatientName);
		
		
		lblStaffName.setBounds(134, 202, 151, 14);
		AddAppointemnt.add(lblStaffName);
		
		AddAppointemnt.add(txtSearchPatient);
		cbYearAddAppointment.setModel(new DefaultComboBoxModel(new String[] {"1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"}));
		cbYearAddAppointment.setBounds(136, 124, 74, 20);
		
		AddAppointemnt.add(cbYearAddAppointment);
		cbMonthAddAppointment.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		cbMonthAddAppointment.setBounds(217, 124, 55, 20);
		
		AddAppointemnt.add(cbMonthAddAppointment);
		cbDayAddAppointment.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		cbDayAddAppointment.setBounds(278, 124, 49, 20);
		
		AddAppointemnt.add(cbDayAddAppointment);
		AddPatient.setLayout(null);
		
		frame.getContentPane().add(AddPatient, "AddPatient");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_3.setBounds(176, 11, 176, 19);
		
		AddPatient.add(label_3);
		label_4.setBounds(84, 84, 27, 14);
		
		AddPatient.add(label_4);
		label_5.setBounds(84, 109, 42, 14);
		
		AddPatient.add(label_5);
		label_6.setBounds(84, 147, 39, 14);
		
		AddPatient.add(label_6);
		label_7.setBounds(84, 172, 30, 14);
		
		AddPatient.add(label_7);
		label_8.setBounds(84, 197, 94, 14);
		
		AddPatient.add(label_8);
		label_9.setBounds(84, 251, 152, 14);
		
		AddPatient.add(label_9);
		label_10.setBounds(84, 298, 76, 14);
		
		AddPatient.add(label_10);
		sdPatientRisk.setBounds(246, 196, 200, 26);
		
		AddPatient.add(sdPatientRisk);
		txtPatientName.setColumns(10);
		txtPatientName.setBounds(246, 78, 211, 20);
		
		AddPatient.add(txtPatientName);
		txtPatientSurname.setColumns(10);
		txtPatientSurname.setBounds(246, 109, 211, 20);
		
		AddPatient.add(txtPatientSurname);
		txtPatientAddress.setColumns(10);
		txtPatientAddress.setBounds(246, 147, 211, 20);
		
		AddPatient.add(txtPatientAddress);
		txtPatientPhone.setColumns(10);
		txtPatientPhone.setBounds(246, 172, 211, 20);
		
		AddPatient.add(txtPatientPhone);
		txtPatientAlergies.setColumns(10);
		txtPatientAlergies.setBounds(246, 248, 211, 20);
		
		AddPatient.add(txtPatientAlergies);
		btnCreatePatient.setBounds(107, 405, 129, 23);
		
		AddPatient.add(btnCreatePatient);
		btnCloseAddPatient.setBounds(246, 405, 89, 23);
		
		AddPatient.add(btnCloseAddPatient);
		cbPatientStatus.setBounds(255, 294, 97, 23);
		
		AddPatient.add(cbPatientStatus);
		lblNewLabel_1.setBounds(84, 50, 46, 14);
		
		AddPatient.add(lblNewLabel_1);
		
		AddPatient.add(txtPatientId);
		
		frame.getContentPane().add(EditAppointment, "UpdateAppointment");
		EditAppointment.setLayout(null);
		lblNewLabel_2.setBounds(74, 52, 46, 14);
		
		EditAppointment.add(lblNewLabel_2);
		lblNewLabel_3.setBounds(74, 89, 86, 14);
		
		EditAppointment.add(lblNewLabel_3);
		lblNewLabel_4.setBounds(74, 132, 86, 14);
		
		EditAppointment.add(lblNewLabel_4);
		lblNewLabel_5.setBounds(74, 185, 46, 14);
		
		EditAppointment.add(lblNewLabel_5);
		btnUpdate.setBounds(137, 255, 89, 23);
		
		EditAppointment.add(btnUpdate);
		btnCancel.setBounds(236, 255, 89, 23);
		
		EditAppointment.add(btnCancel);
		lblUpdateDate.setBounds(183, 52, 197, 14);
		
		EditAppointment.add(lblUpdateDate);
		lblUpdatePatient.setBounds(183, 89, 197, 14);
		
		EditAppointment.add(lblUpdatePatient);
		lblUpdateStaff.setBounds(183, 132, 197, 14);
		
		EditAppointment.add(lblUpdateStaff);
		
		EditAppointment.add(txtUpdateStatus);
	}
	
	public void addActionListeners(ActionListener e){
		btnCheckDate.setActionCommand("CheckAppointmentDate");
		btnCheckDate.addActionListener(e);
		
		btnAddNewAppointment.setActionCommand("AddNewAppointMent");
		btnAddNewAppointment.addActionListener(e);
		
		btnPrintPrescription.setActionCommand("PrintPrescription");
		btnPrintPrescription.addActionListener(e);
		
		btnEditAppointment.setActionCommand("EditAppointment");
		btnEditAppointment.addActionListener(e);
		
		//Register Add Appointment actionListeners
		
		btnSaveAppointment.setActionCommand("SaveAppointment");
		btnSaveAppointment.addActionListener(e);
		
		btnClose.setActionCommand("CloseAddNewAppointmentWindow");
		btnClose.addActionListener(e);
		
		btnSearchStaff.setActionCommand("SearchStaff");
		btnSearchStaff.addActionListener(e);
		
		btnAddPatient.setActionCommand("AddNewPatient");
		btnAddPatient.addActionListener(e);
		
		btnSearchPatient.setActionCommand("SearchPatient");
		btnSearchPatient.addActionListener(e);
		
		//Register AddPatient actionListeners
		
		btnCreatePatient.setActionCommand("CreatePatient");
		btnCreatePatient.addActionListener(e);
		
		btnCloseAddPatient.setActionCommand("CloseAddPatient");
		btnCloseAddPatient.addActionListener(e);
		
		//Register Update Appointment Listeners
		
		btnUpdate.setActionCommand("UpdateAppointent");
		btnUpdate.addActionListener(e);
		
		btnCancel.setActionCommand("CloseUpdateAppointemnt");
		btnCancel.addActionListener(e);
	}
	

	public String getSearchDate(){
		return cbYear.getSelectedItem().toString() + "-" + cbMonth.getSelectedItem().toString() + "-" + cbDays.getSelectedItem().toString();
	}
	
	public String getPatientSearchTerm(){
		return txtSearchPatient.getText();
	}
	
	public String getStaffSearchTerm(){
		return txtSearchStaff.getText();
	}
	
	public String getAppointmentDate(){
		return cbYearAddAppointment.getSelectedItem().toString() + "-" + cbMonthAddAppointment.getSelectedItem().toString() + "-" + cbDayAddAppointment.getSelectedItem().toString();
	}
	
	public boolean ValidateSearchPatient(){
		
//		if (txtPatientAddress.getText().isEmpty() || txtPatientAlergies.getText().isEmpty() ||
//				txtPatientAlergies.getText().isEmpty() || txtPatientName.getText().isEmpty() ||
//				txtPatientPhone.getText().isEmpty() || txtPatientSurname.getText().isEmpty() ||
//				txtUpdateStatus.getText().isEmpty())
//			return false;
		
		try{
			Integer.parseInt(txtSearchPatient.getText());
			if (txtSearchPatient.getText().isEmpty()){
				this.showError("You cannot Search without an ID, please type a number", "Error Searching");
			}
		}catch (NumberFormatException e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public Appointment getAppointmentInfo(){
		Appointment appointment = new Appointment();
		
		appointment.setAppointmentDate(cbYearAddAppointment.getSelectedItem().toString() + "-" + cbMonthAddAppointment.getSelectedItem().toString() + "-" + cbDayAddAppointment.getSelectedItem().toString());
		appointment.setAppointmentPatient(this.patient);
		appointment.setAppointmentUser(this.user);
		appointment.setAppointmentStatus("Sceduled");
		
		return appointment;	
	}
	
	public Appointment getAppointmentInfoForUpdate(){
		Appointment appointment = new Appointment();
		
		appointment.setAppointmentDate(cbYear.getSelectedItem().toString() + "-" + cbMonth.getSelectedItem().toString() + "-" + cbDays.getSelectedItem().toString());
		appointment.setAppointmentPatient(this.patient);
		appointment.setAppointmentUser(this.user);
		appointment.setAppointmentStatus(txtUpdateStatus.getText());
		
		return appointment;	
	}
	
	public boolean setAppointmentInfoForUpdate(){
		
		int index = lsAppointments.getSelectedIndex();
		if (index <= -1){
			return false;
			
		}
		String[] date = null;
		lblUpdateDate.setText(this.appointment.get(index).getAppointmentDate());
		
		lblUpdatePatient.setText(this.appointment.get(index).getAppointmentPatient().getPatientName());
		this.patient = this.appointment.get(index).getAppointmentPatient();
		
		lblUpdateStaff.setText(this.appointment.get(index).getAppointmentUser().getUserName());
		this.user = this.appointment.get(index).getAppointmentUser();
		
		txtUpdateStatus.setText(this.appointment.get(index).getAppointmentStatus());
		return true;
	}
	
	public Patient getPatientInfo(){
		
		Patient patient = new Patient();
		
		patient.setPatientAddress(txtPatientAddress.getText());
		patient.setPatientAllergies(txtPatientAddress.getText());
		patient.setPatientId(Integer.parseInt(txtPatientId.getText()));
		patient.setPatientName(txtPatientName.getText());
		patient.setPatientPhone(txtPatientPhone.getText());
		//patient.setPatientRiskStatus(sdPatientRisk.getValue()); why this is not just a int value? Why byte?
		if (cbPatientStatus.isSelected())
			patient.setPatientStatus(1);
		else
			patient.setPatientStatus(0);
		patient.setPatientSurname(txtPatientSurname.getText());
		
		return patient;
		
	}
	public void setGUIAppointments(Appointment[] appointment){
		// TODO GUI components
		DefaultListModel<String> model = new DefaultListModel<>();
		int count = 0;
		for(Appointment i : appointment){
			this.appointment.add(count, i);;
			
			
			String data = i.getAppointmentDate() + " - " + i.getAppointmentPatient().getPatientName() + " - " + i.getAppointmentUser().getUserName();
			model.addElement(data);
			count++;
		}
		lsAppointments.setModel(model);
	}
	
	public void setGUIPatientRecord(Patient record){
		// TODO GUI components
		this.patient = record;
		lblPatientName.setText(this.patient.getPatientName());
	}
	
	public void setGUIStaffRecord(User user){
		this.user = user;
		lblStaffName.setText(this.user.getUserName());
	}
	
	public void switchCard(String cardName){
		CardLayout cl = (CardLayout)(frame.getContentPane().getLayout());
		cl.show(frame.getContentPane(), cardName);
	}
	
	public void showError(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.ERROR_MESSAGE);
	}
	public void showSuccess(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}
}
