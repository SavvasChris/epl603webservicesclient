package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.Action;
import javax.swing.JSlider;
import javax.swing.JButton;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JCheckBox;

import customDataTypes.Patient;
import javax.swing.JPanel;

public class AddNewPatientUIForm {

	private JFrame frame;

	public AddNewPatientUIForm() {
		initialize();
		frame.setVisible(true);
	}
	JSlider srRisk = new JSlider();
	private JTextField txtName;
	private JTextField txtSurname;
	private JTextField txtAddress;
	private JTextField txtPhone;
	private JTextField txtAlergies;
	private JTextField txtDiagnosis;
	private JTextField txtTreatment;
	private JCheckBox chckbxAlive = new JCheckBox("Alive");
	private JButton btnCreatePatient = new JButton("Create Patient");
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 562, 501);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(85, 11, 373, 417);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
	
		
		JLabel lblNewLabel_1 = new JLabel("Add New Patient");
		lblNewLabel_1.setBounds(92, 0, 176, 19);
		panel.add(lblNewLabel_1);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(0, 55, 27, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Surname");
		lblNewLabel_2.setBounds(0, 98, 42, 14);
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel("Address");
		lblNewLabel_4.setBounds(0, 136, 39, 14);
		panel.add(lblNewLabel_4);

		JLabel lblNewLabel_6 = new JLabel("Phone");
		lblNewLabel_6.setBounds(0, 161, 30, 14);
		panel.add(lblNewLabel_6);

		JLabel lblNewLabel_5 = new JLabel("Risk Status");
		lblNewLabel_5.setBounds(0, 186, 94, 14);
		panel.add(lblNewLabel_5);

		JLabel lblNewLabel_7 = new JLabel("Patient Knowed Alergies");
		lblNewLabel_7.setBounds(0, 240, 152, 14);
		panel.add(lblNewLabel_7);
		
		JLabel lblPatientStatus = new JLabel("Patient Status");
		lblPatientStatus.setBounds(0, 287, 76, 14);
		panel.add(lblPatientStatus);
		
		JLabel Label = new JLabel("Diagnosis");
		Label.setBounds(0, 331, 46, 14);
		panel.add(Label);
		
		JLabel lblNewLabel_10 = new JLabel("Treatment");
		lblNewLabel_10.setBounds(0, 356, 111, 14);
		panel.add(lblNewLabel_10);
		srRisk.setBounds(162, 185, 200, 26);
		panel.add(srRisk);
		
		txtName = new JTextField();
		txtName.setBounds(162, 55, 211, 20);
		panel.add(txtName);
		txtName.setColumns(10);
		
		txtSurname = new JTextField();
		txtSurname.setBounds(162, 98, 211, 20);
		panel.add(txtSurname);
		txtSurname.setColumns(10);
		
		txtAddress = new JTextField();
		txtAddress.setBounds(162, 136, 211, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(162, 161, 211, 20);
		panel.add(txtPhone);
		txtPhone.setColumns(10);
		
		txtAlergies = new JTextField();
		txtAlergies.setBounds(162, 237, 211, 20);
		panel.add(txtAlergies);
		txtAlergies.setColumns(10);
		
		txtDiagnosis = new JTextField();
		txtDiagnosis.setBounds(162, 325, 211, 20);
		panel.add(txtDiagnosis);
		txtDiagnosis.setColumns(10);
		
		txtTreatment = new JTextField();
		txtTreatment.setBounds(162, 353, 211, 20);
		panel.add(txtTreatment);
		txtTreatment.setColumns(10);
		btnCreatePatient.setBounds(63, 394, 89, 23);
		panel.add(btnCreatePatient);
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(162, 394, 89, 23);
		panel.add(btnClose);
		chckbxAlive.setBounds(171, 283, 97, 23);
		panel.add(chckbxAlive);
	}
	
	public void addActionListeners(ActionListener e){
		btnCreatePatient.setActionCommand("CreatePatient");
		btnCreatePatient.addActionListener(e);
		
	}
	
	
	public Patient getInfoFromForm(){
		Patient returnedPatient = null;
		
		returnedPatient.setPatientName(txtName.getText());
		returnedPatient.setPatientSurname(txtSurname.getText());
		returnedPatient.setPatientAddress(txtAddress.getText());
		returnedPatient.setPatientPhone(txtPhone.getText());
		returnedPatient.setPatientAllergies(txtAlergies.getText());
		//returnedPatient.setPatientRiskStatus(srRisk.getValue());
		if (chckbxAlive.isSelected())
			returnedPatient.setPatientStatus(1);
		else
			returnedPatient.setPatientStatus(0);
		
		
		return returnedPatient;
	}
}
