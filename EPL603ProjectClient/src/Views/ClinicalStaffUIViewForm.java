package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.Action;
import javax.swing.JSlider;
import javax.swing.JButton;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JCheckBox;

import customDataTypes.Patient;

public class ClinicalStaffUIViewForm {

	private JFrame frame;

	/**
	 * Launch the application.
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClinicalStaffUI window = new ClinicalStaffUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	} */

	/**
	 * Create the application.
	 */
	public ClinicalStaffUIViewForm() {
		initialize();
		frame.setVisible(true);
	}
	JSlider srRisk = new JSlider();
	private JTextField txtName;
	private JTextField txtSurname;
	private JTextField txtAddress;
	private JTextField txtPhone;
	private JTextField txtAlergies;
	private JTextField txtDiagnosis;
	private JTextField txtTreatment;
	private JButton btnSave = new JButton("Save");
	private JCheckBox chckbxAlive = new JCheckBox("Alive");
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 562, 501);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
	
		
		JLabel lblNewLabel_1 = new JLabel("Receptionis - Update Patient");
		lblNewLabel_1.setBounds(124, 11, 308, 19);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(85, 66, 27, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Surname");
		lblNewLabel_2.setBounds(85, 109, 42, 14);
		frame.getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel("Address");
		lblNewLabel_4.setBounds(85, 147, 39, 14);
		frame.getContentPane().add(lblNewLabel_4);

		JLabel lblNewLabel_6 = new JLabel("Phone");
		lblNewLabel_6.setBounds(85, 172, 30, 14);
		frame.getContentPane().add(lblNewLabel_6);

		JLabel lblNewLabel_5 = new JLabel("Risk Status");
		lblNewLabel_5.setBounds(85, 197, 94, 14);
		frame.getContentPane().add(lblNewLabel_5);

		JLabel lblNewLabel_7 = new JLabel("Patient Knowed Alergies");
		lblNewLabel_7.setBounds(85, 251, 152, 14);
		frame.getContentPane().add(lblNewLabel_7);
		
		JLabel lblPatientStatus = new JLabel("Patient Status");
		lblPatientStatus.setBounds(85, 298, 76, 14);
		frame.getContentPane().add(lblPatientStatus);
		
		JLabel Label = new JLabel("Diagnosis");
		Label.setBounds(85, 342, 46, 14);
		frame.getContentPane().add(Label);
		
		JLabel lblNewLabel_10 = new JLabel("Treatment");
		lblNewLabel_10.setBounds(85, 367, 111, 14);
		frame.getContentPane().add(lblNewLabel_10);

		
		srRisk.setBounds(247, 196, 200, 26);
		frame.getContentPane().add(srRisk);
		
		txtName = new JTextField();
		txtName.setBounds(247, 66, 211, 20);
		frame.getContentPane().add(txtName);
		txtName.setColumns(10);
		
		txtSurname = new JTextField();
		txtSurname.setBounds(247, 109, 211, 20);
		frame.getContentPane().add(txtSurname);
		txtSurname.setColumns(10);
		
		txtAddress = new JTextField();
		txtAddress.setBounds(247, 147, 211, 20);
		frame.getContentPane().add(txtAddress);
		txtAddress.setColumns(10);
		
		txtPhone = new JTextField();
		txtPhone.setBounds(247, 172, 211, 20);
		frame.getContentPane().add(txtPhone);
		txtPhone.setColumns(10);
		
		txtAlergies = new JTextField();
		txtAlergies.setBounds(247, 248, 211, 20);
		frame.getContentPane().add(txtAlergies);
		txtAlergies.setColumns(10);
		
		txtDiagnosis = new JTextField();
		txtDiagnosis.setBounds(247, 336, 211, 20);
		frame.getContentPane().add(txtDiagnosis);
		txtDiagnosis.setColumns(10);
		
		txtTreatment = new JTextField();
		txtTreatment.setBounds(247, 364, 211, 20);
		frame.getContentPane().add(txtTreatment);
		txtTreatment.setColumns(10);
		
		
		btnSave.setBounds(148, 405, 89, 23);
		frame.getContentPane().add(btnSave);
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(247, 405, 89, 23);
		frame.getContentPane().add(btnClose);
		
		
		chckbxAlive.setBounds(256, 294, 97, 23);
		frame.getContentPane().add(chckbxAlive);
	}
	public void addActionListener(ActionListener e){
		btnSave.setActionCommand("SaveExistingPatient");
		btnSave.addActionListener(e);
	}
	public void setInfoFromDB(Patient tempPatient){
		txtName.setText(tempPatient.getPatientName());
		txtSurname.setText(tempPatient.getPatientSurname());
		txtAddress.setText(tempPatient.getPatientAddress());
		txtAlergies.setText(tempPatient.getPatientAllergies());
		txtPhone.setText(String.valueOf(tempPatient.getPatientPhone()));
		if (tempPatient.isPatientStatus() == 1)
			chckbxAlive.setSelected(true);
		else
			chckbxAlive.setSelected(false);
		srRisk.setValue(tempPatient.getPatientRiskStatus());
	}
	
	public Patient getInfoFromForm(){
		Patient returnedPatient = null;
		
		returnedPatient.setPatientName(txtName.getText());
		returnedPatient.setPatientSurname(txtSurname.getText());
		returnedPatient.setPatientAddress(txtAddress.getText());
		returnedPatient.setPatientPhone(txtPhone.getText());
		returnedPatient.setPatientAllergies(txtAlergies.getText());
		//returnedPatient.setPatientRiskStatus(srRisk.getValue());
		if (chckbxAlive.isSelected())
			returnedPatient.setPatientStatus(1);
		else
			returnedPatient.setPatientStatus(0);
		
		
		return returnedPatient;
	}
}
