package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JButton;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.JTextField;

import customDataTypes.Patient;
import customDataTypes.Session;
import customDataTypes.Treatments;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class ClinicalStaffUIView {

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public ClinicalStaffUIView() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private JButton btnConfirmRecord = new JButton("Confirm Record");
	private JButton btnAddNewSession = new JButton("Add New Session");
	private JLabel lblName = new JLabel("New label");
	private JLabel lblSurname = new JLabel("New label");
	private JLabel lblAddress = new JLabel("New label");
	private JLabel lblPhone = new JLabel("New label");
	private JSlider srRisk = new JSlider();
	private JLabel lblAlergies = new JLabel("New label");
	private JLabel lblStatus = new JLabel("New label");
	private JLabel lblRecordStatus = new JLabel("New Label");
	private JButton btnSearchPatient = new JButton("Search Patient");
	private JButton btnSearchSession = new JButton("Search Session");
	private JTextArea txtDiagnosis = new JTextArea();
	private JTextArea txtSideEffects = new JTextArea();
	private JTextArea txtDrugs = new JTextArea();
	private JTextField txtSearchTerm = new JTextField();
	private JTextArea txtNotes = new JTextArea();
	private JComboBox cbYear = new JComboBox();
	private JComboBox cbMonth = new JComboBox();
	private JComboBox cbDay = new JComboBox();
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 671, 775);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
	
		
		JLabel lblNewLabel_1 = new JLabel("Clinical Stuff");
		lblNewLabel_1.setBounds(218, 11, 111, 19);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		frame.getContentPane().add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 103, 635, 562);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(0, 0, 155, 14);
		panel_1.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Surname");
		lblNewLabel_2.setBounds(0, 25, 155, 14);
		panel_1.add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel("Address");
		lblNewLabel_4.setBounds(0, 50, 155, 14);
		panel_1.add(lblNewLabel_4);

		JLabel lblNewLabel_6 = new JLabel("Phone");
		lblNewLabel_6.setBounds(0, 75, 155, 14);
		panel_1.add(lblNewLabel_6);

		JLabel lblNewLabel_5 = new JLabel("Risk Status");
		lblNewLabel_5.setBounds(3, 100, 152, 14);
		panel_1.add(lblNewLabel_5);

		JLabel lblNewLabel_7 = new JLabel("Patient Knowed Alergies");
		lblNewLabel_7.setBounds(3, 127, 152, 14);
		panel_1.add(lblNewLabel_7);
		
		JLabel lblPatientStatus = new JLabel("Patient Status");
		lblPatientStatus.setBounds(3, 152, 212, 14);
		panel_1.add(lblPatientStatus);
		
		JLabel Label = new JLabel("Diagnosis");
		Label.setBounds(0, 203, 215, 14);
		panel_1.add(Label);
		
		JLabel lblNewLabel_10 = new JLabel("Treatment");
		lblNewLabel_10.setBounds(0, 394, 200, 14);
		panel_1.add(lblNewLabel_10);
		
		JLabel lblNewLabel_11 = new JLabel("Record Status");
		lblNewLabel_11.setBounds(0, 177, 222, 14);
		panel_1.add(lblNewLabel_11);
		lblName.setBounds(295, 0, 229, 14);
		panel_1.add(lblName);
		lblSurname.setBounds(295, 25, 229, 14);
		panel_1.add(lblSurname);
		lblAddress.setBounds(295, 50, 229, 14);
		panel_1.add(lblAddress);
		lblPhone.setBounds(295, 75, 229, 14);
		panel_1.add(lblPhone);
		srRisk.setMinimum(1);
		srRisk.setMaximum(10);
		srRisk.setBounds(225, 88, 200, 26);
		panel_1.add(srRisk);
		lblAlergies.setBounds(295, 127, 229, 14);
		panel_1.add(lblAlergies);
		lblStatus.setBounds(295, 152, 229, 14);
		panel_1.add(lblStatus);
		lblRecordStatus.setBounds(295, 177, 229, 14);
		panel_1.add(lblRecordStatus);
		
		JLabel lblNewLabel_8 = new JLabel("Notes");
		lblNewLabel_8.setBounds(3, 337, 212, 14);
		panel_1.add(lblNewLabel_8);
		
		
		txtDiagnosis.setEditable(false);
		txtDiagnosis.setBounds(225, 203, 400, 83);
		panel_1.add(txtDiagnosis);
		
		
		txtDrugs.setBounds(225, 441, 152, 111);
		panel_1.add(txtDrugs);
		
		
		txtSideEffects.setBounds(387, 441, 152, 111);
		panel_1.add(txtSideEffects);
		
		JLabel lblDrugs = new JLabel("Drugs");
		lblDrugs.setBounds(225, 416, 46, 14);
		panel_1.add(lblDrugs);
		
		JLabel lblSideEffects = new JLabel("Side Effects");
		lblSideEffects.setBounds(388, 416, 94, 14);
		panel_1.add(lblSideEffects);
		
		
		txtNotes.setBounds(225, 297, 400, 83);
		panel_1.add(txtNotes);
		btnConfirmRecord.setEnabled(false);
		
		btnConfirmRecord.setBounds(10, 676, 159, 23);
		frame.getContentPane().add(btnConfirmRecord);
		btnAddNewSession.setEnabled(false);
		
		btnAddNewSession.setBounds(179, 676, 183, 23);
		frame.getContentPane().add(btnAddNewSession);
		
		JLabel lblNewLabel_3 = new JLabel("Type ID");
		lblNewLabel_3.setBounds(67, 45, 46, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		
		btnSearchPatient.setBounds(380, 41, 118, 23);
		frame.getContentPane().add(btnSearchPatient);
		
		JLabel lblSessionDate = new JLabel("Session Date");
		lblSessionDate.setBounds(42, 70, 86, 14);
		frame.getContentPane().add(lblSessionDate);
		btnSearchSession.setEnabled(false);
		
		
		btnSearchSession.setBounds(384, 66, 114, 23);
		frame.getContentPane().add(btnSearchSession);
		
		
		txtSearchTerm.setBounds(181, 41, 132, 20);
		frame.getContentPane().add(txtSearchTerm);
		txtSearchTerm.setColumns(10);
		cbYear.setEnabled(false);
		
		
		cbYear.setModel(new DefaultComboBoxModel(new String[] {"1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"}));
		cbYear.setBounds(181, 67, 74, 20);
		frame.getContentPane().add(cbYear);
		cbMonth.setEnabled(false);
		
		
		cbMonth.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		cbMonth.setBounds(265, 67, 55, 20);
		frame.getContentPane().add(cbMonth);
		cbDay.setEnabled(false);
		
		
		cbDay.setModel(new DefaultComboBoxModel(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		cbDay.setBounds(325, 67, 49, 20);
		frame.getContentPane().add(cbDay);
	}

	public void addActionListeners(ActionListener e){
		btnConfirmRecord.setActionCommand("ConfirmRecord");
		btnConfirmRecord.addActionListener(e);
		
		btnAddNewSession.setActionCommand("AddNewSession");
		btnAddNewSession.addActionListener(e);
		
		btnSearchPatient.setActionCommand("SearchPatient");
		btnSearchPatient.addActionListener(e);
		
		btnSearchSession.setActionCommand("SearchSession");
		btnSearchSession.addActionListener(e);
	}
	
	public String getSearchTerm(){
		return txtSearchTerm.getText();
	}
	
	public String getSearchSessionTerm(){
		return cbYear.getSelectedItem().toString() + "-" +cbMonth.getSelectedItem().toString() +"-"+cbDay.getSelectedItem().toString();
	}
	
	public void setGUIPatientRecord(Patient record){
		lblName.setText(record.getPatientName());
		lblSurname.setText(record.getPatientSurname());
		lblAddress.setText(record.getPatientAddress());
		lblAlergies.setText(record.getPatientAllergies());
		lblPhone.setText(String.valueOf(record.getPatientPhone()));
		lblStatus.setText(String.valueOf(record.isPatientStatus()));
		srRisk.setValue(record.getPatientRiskStatus());
		//lblTreatment.setText(tempPatient);
	}
	
	public void setGUISessionInfo(Session sessionInfo){
		txtDiagnosis.setText(sessionInfo.getSessionCondition());
		txtNotes.setText(sessionInfo.getSessionNotes());
		if (sessionInfo.isSessionUptodate() == 0)
			lblRecordStatus.setText("Draft");
		else
			lblRecordStatus.setText("Confirmed");
		
	}
	
	public void setRecordStatus(boolean status){
		if (status)
			lblRecordStatus.setText("Confirmed");
		else
			lblRecordStatus.setText("Draft");
	}
	
	public void changeConfirmRecordVisibility(boolean option){
		btnConfirmRecord.setEnabled(option);
	}
	
	public void changeSearchSessionControls(boolean option){
		btnSearchSession.setEnabled(option);
		cbYear.setEnabled(true);
		cbMonth.setEnabled(true);
		cbDay.setEnabled(true);
	}
	
	public void changeDiagnosisControl(boolean option){
		txtDiagnosis.setEnabled(option);
	}
	
	public void changeNotesSessionControl(boolean option){
		txtNotes.setEnabled(option);
	}
	
	public void changeAddNewSessionControl(boolean option){
		btnAddNewSession.setEnabled(option);
	}
	
	public void setGUITreatments(Treatments treatments){
		String drugString = "";
		String sideEffect = "";
		//lblTreatment.setText(treatments.getName());
		
		for(String i : treatments.getDrug()){
			drugString += i + "\n";
		}
		txtDrugs.setText(drugString);
		
		for(String i : treatments.getSideEffects()){
			sideEffect += i + "\n";
		}
		txtSideEffects.setText(sideEffect);
	}
	
	public void showError(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.ERROR_MESSAGE);
	
	}
	
	public void showSuccess(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void Validate(){
		
		
		try{
			Integer.parseInt(txtSearchTerm.getText());
			if (txtSearchTerm.getText().isEmpty()){
				this.showError("You cannot Search without an ID, please type a number", "Error Searching");
			}
		}catch (NumberFormatException e){
			this.showError("You can only use numbers", "Please user only numbers");
		}
	}
}
