package Views;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;

import customDataTypes.Session;
import customDataTypes.Treatments;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;

import java.awt.event.ItemListener;

public class AddSessionUI {

	private JFrame frame;
	private JTextField txtDate;
	private JButton btnSave = new JButton("Save");
	private JButton btnClose = new JButton("Close");
	private JLabel lblLbldate = new JLabel("lblDate");
	private JComboBox comboBox = new JComboBox();
	private JTextArea txtDiagnosis = new JTextArea();
	private JTextArea txtNotes = new JTextArea();
	private JTextArea txtCondition = new JTextArea();
	private JList lsDrugs = new JList();
	JList lsSideEffects = new JList();
	private Treatments treatments; //keep all appointemnts
	public AddSessionUI() {
		initialize();
		
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 700, 791);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Session Date");
		lblNewLabel.setBounds(91, 59, 78, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Session Treatment");
		lblNewLabel_1.setBounds(91, 106, 97, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Notes");
		lblNewLabel_2.setBounds(91, 256, 97, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		
		btnSave.setBounds(140, 609, 89, 23);
		frame.getContentPane().add(btnSave);
		
		
		btnClose.setBounds(240, 609, 89, 23);
		frame.getContentPane().add(btnClose);
		
		JLabel lblAddNewSession = new JLabel("Add New Session");
		lblAddNewSession.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAddNewSession.setBounds(156, 11, 173, 14);
		frame.getContentPane().add(lblAddNewSession);
		
		
		lblLbldate.setBounds(221, 59, 254, 14);
		frame.getContentPane().add(lblLbldate);
		
		
		comboBox.setBounds(221, 103, 383, 20);
		frame.getContentPane().add(comboBox);
		
		JLabel lblNewLabel_3 = new JLabel("Diagnosis");
		lblNewLabel_3.setBounds(91, 352, 46, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		
		txtDiagnosis.setBounds(221, 322, 383, 77);
		frame.getContentPane().add(txtDiagnosis);
		
		
		txtNotes.setBounds(219, 228, 385, 83);
		frame.getContentPane().add(txtNotes);
		
		JLabel lblNewLabel_4 = new JLabel("Condition");
		lblNewLabel_4.setBounds(91, 432, 46, 14);
		frame.getContentPane().add(lblNewLabel_4);
		
		
		txtCondition.setBounds(225, 432, 379, 77);
		frame.getContentPane().add(txtCondition);
		
		
		lsDrugs.setBounds(221, 144, 173, 73);
		frame.getContentPane().add(lsDrugs);
		
		
		lsSideEffects.setBounds(404, 144, 198, 73);
		frame.getContentPane().add(lsSideEffects);
	}
	
	public void addActionListenters(ActionListener e){
		btnSave.setActionCommand("SaveNewSession");
		btnSave.addActionListener(e);
		
		btnClose.setActionCommand("CloseAddNewSession");
		btnClose.addActionListener(e);
		
		
	}
	
	public void addOnStateChange(ItemListener e){
		comboBox.addItemListener(e);
	}
	
	public void setSessionFormData(Treatments treatment){
		
		DateFormat df = new SimpleDateFormat("yyyy.MM.dd");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);
		lblLbldate.setText(reportDate);
		
		this.treatments = treatment;
		//Savvas changes
		for (String i : treatment.getDrug()){
			comboBox.addItem(i);
		}
		
		
		//Savvas changes end
	}
	
	public int getSelectedIndex(){
		return comboBox.getSelectedIndex();
	}
	
	public void setSideEffects(int index){
		
		DefaultListModel model = new DefaultListModel<>();
		String[] temp = this.treatments.getSideEffects();
		
		model.addElement(temp[index]);
		
		lsSideEffects.setModel(model);
		
	}
	
	public Session getSessionForm(){
		Session tempSession = new Session();
		Treatments treatment = new Treatments();
		
		tempSession.setSessionDate(lblLbldate.getText());
		tempSession.setSessionCondition(txtCondition.getText());
		tempSession.setSessionNotes(txtNotes.getText());
		
		//TODO treatment.setName((String) comboBox.getSelectedItem());
				
		
		String[] sideEffects = new String[10];
		for (int i = 0; i < lsSideEffects.getModel().getSize(); i++) {
			Object item = lsSideEffects.getModel().getElementAt(i);
			sideEffects[i] = (String) item;
		}
		treatment.setSideEffects(sideEffects);
		
		tempSession.setSessionTreatment(treatment);
		
		return tempSession;
	
	}
	
	public boolean Validate(){
		
		if (txtCondition.getText().isEmpty() || txtDiagnosis.getText().isEmpty() || txtNotes.getText().isEmpty()){
			return false;
		}
		
		
		return true;
	}
	public void showError(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.ERROR_MESSAGE);
	}
	public void showSuccess(String msg, String title){
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}
	public void dispose(){
		frame.dispose();
	}
}
