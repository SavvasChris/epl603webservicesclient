package Views;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import customDataTypes.Report;
import javax.swing.JList;

public class ManagerUIView {

	private JFrame frame;
	private JButton btnGenerateReports = new JButton("Generate Reports");
	private JList lsNPEMName = new JList();
	private JList lsNPEMNumber = new JList();
	private JList lsNPEMDate = new JList();
	private JList lblNPEDName = new JList();
	private JList lblNPEDNumber = new JList();
	private JList lblNPEDDate = new JList();
	private JList lsNPECName = new JList();
	private JList lsNPECNumber = new JList();
	private JList lsNPECDate = new JList();
	private JList lsNEDDate = new JList();
	private JList lsNEDDNumber = new JList();
	private JList lsNEDDName = new JList();
	public ManagerUIView() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 813, 614);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		btnGenerateReports.setBounds(188, 11, 164, 58);
		frame.getContentPane().add(btnGenerateReports);
		
		JLabel lblNewLabel = new JLabel("Numbers of Each Drug Descripted");
		lblNewLabel.setBounds(45, 189, 184, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Number of Patients in each condition");
		lblNewLabel_1.setBounds(34, 292, 195, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Number of Patients each day");
		lblNewLabel_2.setBounds(34, 393, 184, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Number of Patient each Month");
		lblNewLabel_3.setBounds(34, 489, 184, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Name");
		lblNewLabel_4.setBounds(261, 103, 46, 14);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Number");
		lblNewLabel_5.setBounds(474, 103, 46, 14);
		frame.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Date");
		lblNewLabel_6.setBounds(645, 103, 46, 14);
		frame.getContentPane().add(lblNewLabel_6);
		
		lsNEDDName.setBounds(260, 155, 164, 89);
		frame.getContentPane().add(lsNEDDName);
		
		lsNEDDNumber.setBounds(434, 155, 164, 89);
		frame.getContentPane().add(lsNEDDNumber);
		
		lsNEDDate.setBounds(608, 155, 164, 89);
		frame.getContentPane().add(lsNEDDate);
		
		lsNPECDate.setBounds(609, 261, 164, 89);
		frame.getContentPane().add(lsNPECDate);
		
		lsNPECNumber.setBounds(435, 261, 164, 89);
		frame.getContentPane().add(lsNPECNumber);
		
		lsNPECName.setBounds(261, 261, 164, 89);
		frame.getContentPane().add(lsNPECName);
		
		lblNPEDDate.setBounds(609, 361, 164, 89);
		frame.getContentPane().add(lblNPEDDate);
		
		lblNPEDNumber.setBounds(435, 361, 164, 89);
		frame.getContentPane().add(lblNPEDNumber);
		
		lblNPEDName.setBounds(261, 361, 164, 89);
		frame.getContentPane().add(lblNPEDName);
		
		lsNPEMDate.setBounds(608, 462, 164, 89);
		frame.getContentPane().add(lsNPEMDate);
		
		lsNPEMNumber.setBounds(434, 462, 164, 89);
		frame.getContentPane().add(lsNPEMNumber);
		
		
		lsNPEMName.setBounds(260, 462, 164, 89);
		frame.getContentPane().add(lsNPEMName);
	}
	
	public void addActionListener(ActionListener e){
		btnGenerateReports.setActionCommand("GenerateReports");
		btnGenerateReports.addActionListener(e);
	}
	
	public void setReportData(Report data){
		
		
	
		//NUMBER OF EACH DRUG PRESCRIPED
		DefaultListModel model_1 = new DefaultListModel<>();
		
		for (String i : data.numberOfEachDrugPrescribed.getName()){
			model_1.addElement(i);
		}
		lsNEDDName.setModel(model_1);
		
		DefaultListModel model_2 = new DefaultListModel<>();
		
		
		for(int i : data.numberOfEachDrugPrescribed.getNumber()){
			model_2.addElement(i);
		}
		lsNEDDNumber.setModel(model_2);

		DefaultListModel model_3 = new DefaultListModel<>();
		
		for(String i : data.numberOfEachDrugPrescribed.getDate()){
			model_3.addElement(i);
		}
		lsNEDDate.setModel(model_3);
	
		//NUMBER OF PATIENTS IN EACH CONDITION
		DefaultListModel model_4 = new DefaultListModel<>();
		for (String i : data.numberOfPatientsEachCondition.getName()){
			model_4.addElement(i);
		}
		lsNPECName.setModel(model_4);
		
		DefaultListModel model_5 = new DefaultListModel<>();
		
		for(int i : data.numberOfPatientsEachCondition.getNumber()){
			model_5.addElement(i);
		}
		lsNPECNumber.setModel(model_5);

		DefaultListModel model_6 = new DefaultListModel<>();
		for(String i : data.numberOfPatientsEachCondition.getDate()){
			model_6.addElement(i);
		}
		lsNPECDate.setModel(model_6);
		
		//NUMBER OF PATIENT EACH DAY
		
		DefaultListModel model_7 = new DefaultListModel<>();
		for (String i : data.numberOfPatientsEachCondition.getName()){
			model_7.addElement(i);
		}
		lblNPEDName.setModel(model_7);
		
		DefaultListModel model_8 = new DefaultListModel<>();
		
		for(int i : data.numberOfPatientsEachCondition.getNumber()){
			model_8.addElement(i);
		}
		lblNPEDNumber.setModel(model_8);

		DefaultListModel model_9 = new DefaultListModel<>();
		for(String i : data.numberOfPatientsEachCondition.getDate()){
			model_9.addElement(i);
		}
		lblNPEDDate.setModel(model_9);
		
		//NUMBER OF PATIENT EACH MONTH
		DefaultListModel model_10 = new DefaultListModel<>();
		for (String i : data.numberOfPatientsEachCondition.getName()){
			model_10.addElement(i);
		}
		lsNPEMName.setModel(model_10);
		
		DefaultListModel model_11 = new DefaultListModel<>();
		
		for(int i : data.numberOfPatientsEachCondition.getNumber()){
			model_11.addElement(i);
		}
		lsNPEMNumber.setModel(model_11);

		DefaultListModel model_12 = new DefaultListModel<>();
		for(String i : data.numberOfPatientsEachCondition.getDate()){
			model_12.addElement(i);
		}
		lsNPEMDate.setModel(model_12);
		
		
		
	}
}
