package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Component;

import javax.swing.Box;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class AddNewApointmentUIForm {

	private JFrame frame;
	private JTextField txtSearch;
	private JButton btnSearch = new JButton("Search Patient");
	private JButton btnAddpatient = new JButton("Add Patient");
	private JButton btnSearchStaff = new JButton("Search Staff");
	private JButton btnAddStaff = new JButton("Add Staff");
	private JButton btnSaveAppointment = new JButton("Save Appointment");
	private JButton btnClose = new JButton("Close");
	private final JPanel panel = new JPanel();

	public AddNewApointmentUIForm() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 674, 435);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		panel.setBounds(39, 10, 585, 183);
		
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		txtSearch = new JTextField();
		txtSearch.setBounds(0, 1, 155, 20);
		panel.add(txtSearch);
		txtSearch.setColumns(10);
		btnSearch.setBounds(165, 0, 110, 23);
		panel.add(btnSearch);
		
		JLabel lblNewLabel = new JLabel("Date");
		lblNewLabel.setBounds(0, 38, 46, 14);
		panel.add(lblNewLabel);
		btnAddpatient.setBounds(285, 0, 89, 23);
		panel.add(btnAddpatient);
		
		JLabel lblNewLabel_1 = new JLabel("Patient Name");
		lblNewLabel_1.setBounds(0, 63, 89, 14);
		panel.add(lblNewLabel_1);
		btnSearchStaff.setBounds(384, 0, 102, 23);
		panel.add(btnSearchStaff);
		btnAddStaff.setBounds(496, 0, 89, 23);
		panel.add(btnAddStaff);
		
		JLabel lblNewLabel_2 = new JLabel("Staff Name");
		lblNewLabel_2.setBounds(0, 88, 73, 14);
		panel.add(lblNewLabel_2);
		btnSaveAppointment.setBounds(0, 160, 135, 23);
		panel.add(btnSaveAppointment);
		btnClose.setBounds(145, 160, 89, 23);
		panel.add(btnClose);
	}
	
	public void addActionListener(ActionListener e){
		btnSearch.setActionCommand("SearchPatient");
		btnSearch.addActionListener(e);
		
		btnAddpatient.setActionCommand("AddPatient");
		btnAddpatient.addActionListener(e);
		
		btnSearchStaff.setActionCommand("SearchStaff");
		btnSearchStaff.addActionListener(e);
		
		btnSaveAppointment.setActionCommand("SaveAppointment");
		btnSaveAppointment.addActionListener(e);
		
		btnClose.setActionCommand("CloseAddNewAppointment");
		btnClose.addActionListener(e);
		
	}
	
	public String getSearchTerm(){
		return txtSearch.getText();
	}
}
