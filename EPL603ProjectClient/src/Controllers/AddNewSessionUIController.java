package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import customDataTypes.Patient;
import customDataTypes.Session;
import customDataTypes.User;
import viewpoints.ClinicalStaff;
import Views.AddSessionUI;

public class AddNewSessionUIController implements ActionListener, ItemListener{
	
	AddSessionUI view;
	ClinicalStaff model;
	
	Patient patient = new Patient();
	User user = new User();
	
	
	public AddNewSessionUIController(AddSessionUI view, ClinicalStaff clinicalModel, Patient patient, User user){
		this.view = view;
		this.model = clinicalModel;
		this.patient = patient;
		this.user = user;
		
		view.addActionListenters(this);
		view.addOnStateChange(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String action_com = e.getActionCommand();
		
		if (action_com == "SaveNewSession"){
			if (view.Validate()){
				Session session = new Session();
				session = view.getSessionForm();
				try {
					model.setSessionInfo(session, patient.getPatientId(), user.getUserId());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
				
				view.showSuccess("Session Saved Successfullt", "Session Saved");
				view.dispose();
			}else{
				view.showError("There seems to be problems on the form", "Error");
			}
		}else if(action_com == "CloseAddNewSession"){
			view.dispose();
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		view.setSideEffects(view.getSelectedIndex());
	}

	
	
	

}
