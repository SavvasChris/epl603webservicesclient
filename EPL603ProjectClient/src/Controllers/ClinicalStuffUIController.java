package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import customDataTypes.Patient;
import customDataTypes.Session;
import customDataTypes.Treatments;
import customDataTypes.User;
import viewpoints.ClinicalStaff;
import viewpoints.MedicalRecords;
import Views.AddSessionUI;
import Views.ClinicalStaffUIView;

public class ClinicalStuffUIController implements ActionListener, ListSelectionListener{
	
	MedicalRecords modelMedicalRecords;
	ClinicalStaff model;
	ClinicalStaffUIView view;
	
	User currentUser = new User();
	Patient currentPatient = new Patient();
	Session currentSession = new Session();
	Treatments currentTreatments = new Treatments();
	
	
	public ClinicalStuffUIController(ClinicalStaff model, ClinicalStaffUIView view, MedicalRecords model_2, User user){
		this.model = model;
		this.view = view;
		this.modelMedicalRecords = model_2;
		this.currentUser = user;
		
		view.addActionListeners(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String action_com = e.getActionCommand();
		
		if (action_com == "ConfirmRecord"){
			currentSession.setSessionUptodate(1);
			try {
				model.setSessionInfo(currentSession, currentPatient.getPatientId(), currentUser.getUserId());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			view.setGUISessionInfo(currentSession);
			
		}else if (action_com == "SearchSession"){
			view.Validate();
			System.out.println(view.getSearchSessionTerm());
			try {
				currentSession = model.getSessionInfo(currentPatient.getPatientId(), view.getSearchSessionTerm());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (currentSession != null){
				view.setGUISessionInfo(currentSession);
				view.setGUITreatments(currentSession.getSessionTreatment());
				if (currentUser.getUserId().equals(currentSession.getUserId())){ //if the user that is logged in is the same as the session user then allow him to confirm or edit diagonosis & notes
					view.changeConfirmRecordVisibility(true);
					view.changeAddNewSessionControl(true);
					view.changeDiagnosisControl(true);
					view.changeNotesSessionControl(true);
				}else{ //else he is not allowed to confirm or edit diagnosis
					view.changeConfirmRecordVisibility(false);
					view.changeDiagnosisControl(false);
				}
			}else{
				view.showError("No Session Found", "Session Not Found");
			}
			
		}else if (action_com == "SearchPatient"){
			view.Validate();
			try {
				currentPatient = model.getPatientRecord(Integer.parseInt(view.getSearchTerm()));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if(currentPatient != null){
				view.setGUIPatientRecord(currentPatient);
				view.changeSearchSessionControls(true);
			}else{
				view.showError("Patient not found", "Patient Not Found");
			}
		}else if (action_com == "AddNewSession"){
			//START ADD NEW SESSION MODEL, CONTROLLER, VIEW
			AddSessionUI addSessionview = new AddSessionUI();
			
			try {
				addSessionview.setSessionFormData(model.getTreatmentsInfo());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			AddNewSessionUIController controller = new AddNewSessionUIController(addSessionview, model, currentPatient, currentUser);
			//view.setGUISessionInfo(model.getSessionInfo()); //get the session that was newlu created
			
			
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		System.out.println("changed");
	}
	

}
