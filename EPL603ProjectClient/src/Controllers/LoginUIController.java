package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import viewpoints.ClinicalStaff;
import viewpoints.Management;
import viewpoints.MedicalRecords;
import viewpoints.Receptionists;
import customDataTypes.LoginInfo;
import customDataTypes.User;
import Views.ClinicalStaffUIView;
import Views.LoginUIView;
import Views.ManagerUIView;
import Views.ReceptionistUIView;

public class LoginUIController implements ActionListener{
	
	MedicalRecords model;
	LoginUIView view;
	
	public LoginUIController(MedicalRecords model, LoginUIView view){
		
		this.model = model;
		this.view = view;
		view.addActionListenerbtnLogin(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String action_com = e.getActionCommand();
		User user = null;
		
		if (action_com == "Login"){
			if (view.Validate())
			{
				LoginInfo data = new LoginInfo();
				data = view.getLoginFormData();
				try {
					user = model.setUser(data.getUsername(), data.getPassword());
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				if(user == null){ //user not found
					view.showError("This user does not exist, please try again", "User not found");
				}else{ //user found
				
					switch (user.getUserType()){ //if Clinical Staff
					
						case 1:{
							view.setVisible(false);
							ClinicalStaffUIView clinicalStaffview = new ClinicalStaffUIView();
							ClinicalStaff clinicalStuffModel = new ClinicalStaff();
							ClinicalStuffUIController clinicalStuffController = new ClinicalStuffUIController(clinicalStuffModel, clinicalStaffview, model,user);
							break;
							
						}
						case 2:{ // If Receptionist
							view.setVisible(false);
							ReceptionistUIView receptionistsView = new ReceptionistUIView();
							Receptionists model = new Receptionists();
							ReceptionistsUIController controller = new ReceptionistsUIController(receptionistsView, model);
							break;
							
						}
						case 3:{ // if Manager
							
							ManagerUIView view = new ManagerUIView();
							Management model = new Management();
							ManagerUIController controller = new ManagerUIController(model,view);
							break;
							
						}		
					}
				}
			}
		}else{ // Other actions
			
		}
	}
}	


