package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import customDataTypes.Appointment;
import customDataTypes.Patient;
import customDataTypes.User;
import viewpoints.MedicalRecords;
import viewpoints.Receptionists;
import Views.ReceptionistUIView;

public class ReceptionistsUIController implements ActionListener{
	
	ReceptionistUIView view;
	Receptionists model;
	MedicalRecords medicalRecordsModel = new MedicalRecords();
	
	Appointment[] currentAppointments;
	
	public ReceptionistsUIController(ReceptionistUIView view, Receptionists model){
		this.view = view;
		this.model = model;
		
		view.addActionListeners(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		String action_com = e.getActionCommand();
		
		if (action_com == "CheckAppointmentDate"){ //check appointment by date. if exist publish
			try {
				currentAppointments = model.getAppointments(view.getSearchDate());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} //get the appointments from model
			
			view.setGUIAppointments(currentAppointments); //sent the appointments to view so the can be displayed
		}else if (action_com == "AddNewAppointMent"){
			view.switchCard("AddAppointment");
		}else if(action_com == "EditAppointment"){
			if (view.setAppointmentInfoForUpdate() == false){
				view.showError("You didnt select any appointment to edit", "Error");
			}else{
				view.switchCard("UpdateAppointment");
			}
		}else if(action_com == "CloseAddNewAppointmentWindow"){//if user clicks close, switch card to appointment
			view.switchCard("Appointment");
		}else if(action_com == "SaveAppointment"){//if user clicks save, use the model and sent the Appointment object to be saved, then switch card
			try {
				model.setPatientAppointment(view.getAppointmentInfo());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			view.switchCard("Appointment");
		}else if (action_com == "SearchPatient"){ //seatch Patient based on the input in the TextBox
			Patient patient = null;
			
			if (view.ValidateSearchPatient()){
				try {
					patient = model.getPatientRecord(Integer.parseInt(view.getPatientSearchTerm()));
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (patient != null) //if patient found then update GUI
					view.setGUIPatientRecord(patient);
				else //if not found show error
					view.showError("No Patient found with this ID", "Patient Not Found");
			}else{
				view.showError("You have errors please try again", "Error");
			}
			
			
		}else if (action_com == "SearchStaff"){
			User user = new User();
			
			try {
				user = medicalRecordsModel.getUser(view.getStaffSearchTerm());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			if (user != null)
				view.setGUIStaffRecord(user);
			else
				view.showError("No Staff Member Found with this ID", "User Not Found");
		}else if (action_com == "AddNewPatient"){//if user clicks add patient, switch to card for input
			view.switchCard("AddPatient");
		}else if (action_com == "CreatePatient"){//if user clicks create patient call the model and save patient to DB, then return to previous screen
			try {
				model.setPatient(view.getPatientInfo());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			view.switchCard("Appointment");
		}else if (action_com == "CloseAddPatient"){ //if user closes the add patient return to previes window
			view.switchCard("Appointment");
		}else if (action_com == "UpdateAppointent"){
			try {
				model.setPatientAppointment(view.getAppointmentInfoForUpdate());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			view.showSuccess("Successfully Updated", "Success");
			view.switchCard("Appointment");
		}else if (action_com == "CloseUpdateAppointemnt"){
			view.switchCard("Appointment");
		}
		
	}


}
