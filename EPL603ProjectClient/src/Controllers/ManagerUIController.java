package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import customDataTypes.Report;
import Views.ManagerUIView;
import viewpoints.Management;

public class ManagerUIController implements ActionListener{
	
	Management model;
	ManagerUIView view;
	
	public ManagerUIController(Management model, ManagerUIView view){
		this.model = model;
		this.view = view;
		
		view.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String action_com = e.getActionCommand();
		
		if (action_com == "GenerateReports"){
			Report report = null;
			
			try {
				report = model.createReport();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			view.setReportData(report);
		}
	}
	
	

}
